import { AnchorHTMLAttributes, PropsWithChildren, ReactElement } from 'react';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import Link from 'next/link';

type Props = PropsWithChildren<{
  href: string;
  className?: string;
  activeClassName?: string;
  htmlProps?: AnchorHTMLAttributes<HTMLAnchorElement>;
}>;

const NavLink = ({ children, href, className = '', activeClassName = 'active', htmlProps }: Props): ReactElement => {
  const { asPath, pathname } = useRouter();

  return (
    <Link href={href}>
      <a
        className={clsx(className, { [activeClassName]: pathname === href || asPath.split('?')?.[0] === href })}
        {...htmlProps}
      >
        {children}
      </a>
    </Link>
  );
};

export default NavLink;
