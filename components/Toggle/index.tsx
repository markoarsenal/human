import Switch from 'react-switch';

export type ToggleProps = {
  value: boolean;
  onChange: (checked: boolean) => void;
};

const Toggle = ({ value, onChange }: ToggleProps) => {
  return (
    <Switch
      checked={value}
      onChange={(val) => onChange(val)}
      offColor="#aaa"
      onColor="#21c35e"
      handleDiameter={30}
      uncheckedIcon={false}
      checkedIcon={false}
      boxShadow="0 0 5px rgba(0, 0, 0, 0.25)"
      activeBoxShadow="0 0 5px rgba(0, 0, 0, 0.25)"
      height={25}
      width={50}
    />
  );
};

export default Toggle;
