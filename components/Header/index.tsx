import { useContext, useState } from 'react';
import { categories as categoriesConfig } from '../../config';
import { NewsContext } from '../../context/news';
import Dropdown from '../Dropdown';
import NavLink from '../NavLink';
import Toggle from '../Toggle';

const Header = () => {
  const { categories, toggleCategory } = useContext(NewsContext);

  return (
    <nav className="mb-10 sticky top-0 bg-sky-900 z-20 shadow">
      <div className="container flex items-center">
        <ul className="flex grow text-white">
          <li className="basis-1/5">
            <NavLink
              href="/"
              className="block p-4 text-center transition-colors hover:bg-sky-800"
              activeClassName="bg-sky-800"
            >
              Show all
            </NavLink>
          </li>
          {categories &&
            Object.entries(categories).map(([categoryId, val]) => {
              return (
                <li className="basis-1/5" key={categoryId}>
                  <NavLink
                    href={`/category/${categoryId}`}
                    className="block p-4 text-center transition-colors hover:bg-sky-800"
                    activeClassName="bg-sky-800"
                  >
                    {val.name}
                  </NavLink>
                </li>
              );
            })}
        </ul>
        {categories && (
          <Dropdown
            items={Object.entries(categories).map(([categoryId, val]) => {
              return (
                <div className="flex items-center">
                  <div className="grow">{val.name}</div>
                  <Toggle value={val.show} onChange={(checked) => toggleCategory(categoryId, checked)} />
                </div>
              );
            })}
            className="grow-0 shrink-0 ml-4"
          />
        )}
      </div>
    </nav>
  );
};

export default Header;
