import { useEffect, useState } from 'react';
import Button from '../Button';

type SearchProps = {
  initialValue?: string;
  onSearch: (query: string) => void;
};

const Search = ({ initialValue = '', onSearch }: SearchProps) => {
  const [search, setSearch] = useState(initialValue);

  useEffect(() => {
    setSearch(initialValue);
    onSearch(initialValue);
  }, [initialValue]);

  return (
    <form
      className="flex items-center w-1/2 mx-auto mb-4"
      onSubmit={(e) => {
        e.preventDefault();
        const query = search.trim();
        if (query.length > 2 || query.length === 0) onSearch(query);
      }}
    >
      <input
        className="grow h-10 px-3 rounded-l-sm outline-none"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
      <Button className="rounded-l-none" size="large" variant="green" type="submit">
        Search
      </Button>
    </form>
  );
};

export default Search;
