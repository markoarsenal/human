import { ReactNode, useState } from 'react';
import clsx from 'clsx';
import useOuterClick from '../../hooks/useOuterClick';
import SettingsIcon from '../../assets/icons/settings.svg';
import Button from '../Button';

type DropdownProps = {
  items: ReactNode[];
  triggerBtnContent?: ReactNode;
  className?: string;
};

const Dropdown = ({ items, triggerBtnContent, className }: DropdownProps) => {
  const [show, setShow] = useState(false);
  const ref = useOuterClick<HTMLDivElement>(() => setShow(false));

  return (
    <div className={clsx('relative', className)} ref={ref}>
      <Button onClick={() => setShow(!show)}>{triggerBtnContent || <SettingsIcon width={18} />}</Button>
      <ul
        className={clsx(
          'absolute top-full right-0 mt-1 w-72 px-6 py-4 rounded-sm bg-sky-50 z-20',
          show ? 'block' : 'hidden',
        )}
      >
        {items.map((item, i) => {
          return (
            <li className={clsx(i !== 0 && 'mt-3')} key={i}>
              {item}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Dropdown;
