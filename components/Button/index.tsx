import { ButtonHTMLAttributes, PropsWithChildren, ReactElement, useState } from 'react';
import clsx, { ClassValue } from 'clsx';

export type ButtonVariant = 'black' | 'white' | 'green' | 'red' | 'blue';
export type ButtonSize = 'large' | 'regular' | 'small' | 'xsmall';
export type ButtonRounded = 'rounded-sm' | 'rounded' | 'rounded-md' | 'rounded-full';

export type ButtonProps = {
  variant?: ButtonVariant;
  circle?: boolean;
  bordered?: boolean;
  rounded?: ButtonRounded;
  size?: ButtonSize;
} & ButtonHTMLAttributes<HTMLButtonElement>;

const baseVariantStyle = (
  disabled?: boolean,
  circle?: boolean,
  bordered?: boolean,
  size?: ButtonSize,
  rounded?: ButtonRounded,
) =>
  clsx(
    'flex justify-center items-center whitespace-nowrap fill-current overflow-hidden',
    disabled ? 'cursor-not-allowed' : 'cursor-pointer',
    circle ? 'rounded-full' : rounded || 'rounded-sm',
    bordered && 'border',
    size === 'large' && (circle ? 'w-16 h-16' : 'h-10 px-5 py-4'),
    size === 'regular' && (circle ? 'w-12 h-12' : 'h-8 px-3 py-2'),
    size === 'small' && (circle ? 'w-8 h-8' : 'h-6 px-2 py-1'),
    size === 'xsmall' && (circle ? 'w-7 h-7' : 'h-5 px-1.5 py-1'),
    rounded && size === 'large' && 'px-6',
    rounded && size === 'regular' && 'px-4',
    rounded && size === 'small' && 'px-2',
  );

const buttons = (
  disabled?: boolean,
  circle?: boolean,
  bordered?: boolean,
  size?: ButtonSize,
  rounded?: ButtonRounded,
): Record<ButtonVariant, ClassValue[]> => ({
  black: [
    baseVariantStyle(disabled, circle, bordered, size, rounded),
    disabled && 'bg-opacity-10',
    bordered ? 'border-black text-black' : 'bg-black text-white',
  ],
  white: [
    baseVariantStyle(disabled, circle, bordered, size, rounded),
    'shadow',
    disabled && 'text-grey-99 bg-grey-ee',
    bordered ? 'border-grey-cc text-black' : 'bg-white text-black',
  ],
  green: [
    baseVariantStyle(disabled, circle, bordered, size, rounded),
    disabled && 'bg-opacity-40',
    bordered ? 'border-green text-green' : 'bg-green-600 text-white',
  ],
  red: [
    baseVariantStyle(disabled, circle, bordered, size, rounded),
    disabled && 'bg-opacity-40',
    bordered ? 'border-red-700 text-red-700' : 'bg-red-700 text-white',
  ],
  blue: [
    baseVariantStyle(disabled, circle, bordered, size, rounded),
    disabled && 'bg-opacity-40',
    bordered ? 'border-sky-700 text-sky-700' : 'bg-sky-700 text-white',
  ],
});

const Button = ({
  type = 'button',
  disabled,
  circle,
  bordered,
  variant = 'black',
  size = 'regular',
  rounded,
  className,
  children,
  onClick,
  ...htmlButtonProps
}: PropsWithChildren<ButtonProps>): ReactElement => {
  return (
    <button
      type={type}
      disabled={disabled}
      className={clsx(variant && buttons(disabled, circle, bordered, size, rounded)[variant], className)}
      onClick={onClick}
      {...htmlButtonProps}
    >
      {children}
    </button>
  );
};

export default Button;
