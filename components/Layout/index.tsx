import { PropsWithChildren } from 'react';
import Header from '../Header';

type LayoutProps = PropsWithChildren<{}>;

const Layout = ({ children }: LayoutProps) => {
  return (
    <div>
      <Header />
      <main className="container">
        <div className="pb-20">{children}</div>
      </main>
    </div>
  );
};

export default Layout;
