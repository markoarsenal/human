import { useContext } from 'react';
import { NewsContext } from '../../context/news';
import NewsListItem from './NewsListItem';

export type News = {
  title: string;
  slug: string;
  date: string;
  excerpt: string;
  post_image: string;
  post_thumbnail: string;
  post_category_id: string;
};

type NewsListProps = {
  news: News[];
  showRefetch?: boolean;
  onRemove?: (slug: string) => void;
  onRefetch?: () => void;
};

const NewsList = ({ news, showRefetch = false, onRemove, onRefetch }: NewsListProps) => {
  return (
    <div>
      <p className="mb-4 text-white">
        Showing {news.length} items
        {showRefetch && (
          <>
            <span className="mx-2">|</span>
            <button type="button" className="rounded hover:underline" onClick={onRefetch}>
              refetch
            </button>
          </>
        )}
      </p>
      <section className="grid grid-cols-4 gap-6">
        {news.map((item) => {
          return <NewsListItem {...item} key={item.slug} onRemove={onRemove} />;
        })}
      </section>
    </div>
  );
};

export default NewsList;
