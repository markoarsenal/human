import Image from 'next/image';
import Link from 'next/link';
import { News } from '.';
import { truncate } from '../../utils';
import TrashIcon from '../../assets/icons/trash.svg';
import Button from '../Button';
import { categories } from '../../config';

type NewsListItemProps = News & {
  onRemove?: (slug: string) => void;
};

const NewsListItem = ({ title, post_thumbnail, slug, post_category_id, onRemove }: NewsListItemProps) => {
  return (
    <article className="flex flex-col rounded-sm bg-sky-50 overflow-hidden group">
      <div className="relative" style={{ paddingTop: `${(9 / 16) * 100}%` }}>
        <Button
          className="absolute top-2 right-2 z-10 hidden group-hover:block"
          variant="red"
          onClick={() => onRemove?.(slug)}
        >
          <TrashIcon width={16} />
        </Button>
        <div className="absolute inset-0">
          <Link href={`/post/${slug}`}>
            <a>
              <Image
                src={`https://www.alpha-orbital.com/assets/images/post_img/${post_thumbnail}`}
                layout="fill"
                objectFit="cover"
              />
            </a>
          </Link>
        </div>
      </div>
      <div className="flex flex-col grow px-4 py-2">
        <h4 className="mb-2 text-xs text-sky-600">
          <Link href={`/category/${post_category_id}`}>
            <a>{categories[post_category_id]}</a>
          </Link>
        </h4>
        <Link href={`/post/${slug}`}>
          <a>
            <h2 className="mb-4 leading-5 transition-colors hover:text-sky-700">{truncate(title, 50)}</h2>
          </a>
        </Link>
        <Link href={`/post/${slug}`}>
          <a className="mt-auto ml-auto">
            <Button variant="blue">Full article</Button>
          </a>
        </Link>
      </div>
    </article>
  );
};

export default NewsListItem;
