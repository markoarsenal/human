import { useRouter } from 'next/router';
import { useContext, useEffect } from 'react';
import { NewsContext } from '../../context/news';
import { reroute } from '../../utils';
import Search from '../Search';
import NewsList from './';

type NewsListContainerProps = {
  categoryId?: string;
  showRefetch?: boolean;
};

const NewsListContainer = ({ categoryId, showRefetch = false }: NewsListContainerProps) => {
  const { news, categories, removedItems, refetch, remove, search } = useContext(NewsContext);
  const newsData = categoryId ? news.filter((item) => item.post_category_id === categoryId) : news;
  const { query, push } = useRouter();
  const showRefetchBtn =
    showRefetch && (removedItems.length > 0 || !!Object.values(categories).find(({ show }) => !show));

  useEffect(() => {
    categoryId && !categories[categoryId] && push('/');
  }, [news]);

  return (
    <div>
      <Search
        initialValue={query.query as string}
        onSearch={(query) => {
          const params = new URLSearchParams();

          params.set('query', query);
          search(query);
          reroute(query ? `?${params.toString()}` : '');
        }}
      />
      <NewsList news={newsData} showRefetch={showRefetchBtn} onRefetch={refetch} onRemove={remove} />
    </div>
  );
};

export default NewsListContainer;
