import create from 'zustand';
import { News } from '../utils/get-news';

const useNews = create<{
  news: News[];
  setNews: (news: News[]) => void;
  removeNews: (slug: string) => void;
}>((set) => ({
  news: [],
  setNews: (news) => set(() => ({ news })),
  removeNews: (slug) => set((state) => ({ news: state.news.filter((item) => item.slug !== slug) })),
}));

export default useNews;
