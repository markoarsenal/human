import router from 'next/router';

export const truncate = (str: string, length: number, after = '...', cutWords = false) => {
  if (str.length <= length) return str;

  let result = str.substring(0, length);
  if (!cutWords) result = result.substring(0, Math.min(result.length, result.lastIndexOf(' ')));

  return `${result}${after}`;
};

export const areObjectsSame = (x: Object, y: Object) => {
  let objectsAreSame = true;

  for (let propertyName in x) {
    if (x[propertyName] !== y[propertyName]) {
      objectsAreSame = false;
      break;
    }
  }

  return objectsAreSame;
};

export const getUrlParamValue = (name: string): string | null =>
  typeof window !== 'undefined' ? new URLSearchParams(location.search).get(name) : null;

export const reroute = (params: string): void => {
  setTimeout(
    () =>
      typeof window !== 'undefined' &&
      router.replace(params ? location.pathname + params : router.asPath, undefined, {
        scroll: false,
      }),
    200,
  );
};
