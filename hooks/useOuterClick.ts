import { MutableRefObject, useEffect, useRef } from 'react';

const eventName = 'click';

const useOuterClick = <T extends HTMLElement>(callback: (e: Event) => void): MutableRefObject<T | null> => {
  const callbackRef = useRef<(e: Event) => void>();
  const innerRef = useRef<T>(null);

  useEffect(() => {
    callbackRef.current = callback;
  });

  useEffect(() => {
    const container = document.body;
    const handleClick = (e: Event) =>
      e.target instanceof Element && !innerRef.current?.contains(e.target) && callbackRef.current?.(e);
    container.addEventListener(eventName, handleClick);
    return () => container.removeEventListener(eventName, handleClick);
  }, []);

  return innerRef;
};

export default useOuterClick;
