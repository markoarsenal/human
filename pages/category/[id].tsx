import { useRouter } from 'next/router';
import NewsListContainer from '../../components/NewsList/NewsListContainer';

const Category = () => {
  const {
    query: { id },
  } = useRouter();

  return <NewsListContainer categoryId={id as string} />;
};

export default Category;
