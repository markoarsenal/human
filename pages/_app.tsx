import App, { AppProps } from 'next/app';
import Layout from '../components/Layout';
import { News } from '../components/NewsList';
import NewsProvider from '../context/news';
import '../styles/globals.css';

function MyApp({ Component, pageProps, news }: AppProps & { news: News[] }) {
  return (
    <NewsProvider news={news}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </NewsProvider>
  );
}

MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext);
  const news: News[] = await fetch('https://www.alpha-orbital.com/last-100-news.json').then((res) => res.json());

  return { ...appProps, news };
};

export default MyApp;
