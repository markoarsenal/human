import { useContext } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';
import { NewsContext } from '../../context/news';
import { categories } from '../../config';

const Post = () => {
  const {
    query: { slug },
  } = useRouter();
  const { news } = useContext(NewsContext);
  const { title, excerpt, date, post_category_id, post_image } = news.find((item) => item.slug === slug);

  return (
    <div className="container text-white">
      <div className="relative mb-4" style={{ paddingTop: `${(9 / 16) * 100}%` }}>
        <Image
          src={`https://www.alpha-orbital.com/assets/images/post_img/${post_image}`}
          layout="fill"
          objectFit="cover"
        />
      </div>
      <div className="flex justify-end mb-2 text-xs">
        <time className="mr-4">{date}</time>
        <h4 className="text-green-500 uppercase">
          <Link href={`/category/${post_category_id}`}>
            <a>{categories[post_category_id]}</a>
          </Link>
        </h4>
      </div>
      <h1 className="text-2xl mb-4">{title}</h1>
      <div dangerouslySetInnerHTML={{ __html: excerpt }} />
    </div>
  );
};

export default Post;
