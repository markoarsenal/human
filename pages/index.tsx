import NewsListContainer from '../components/NewsList/NewsListContainer';

const Home = () => {
  return <NewsListContainer showRefetch />;
};

export default Home;
