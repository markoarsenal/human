import { createContext, PropsWithChildren, useCallback, useEffect, useRef, useState } from 'react';
import { News } from '../components/NewsList';
import { categories as categoriesConfig } from '../config';
import { areObjectsSame } from '../utils';

type NewsProviderProps = PropsWithChildren<{ news: News[] }>;
type Categories = Record<string, { name: string; show: boolean }>;

const getCategories = (data: News[]): Categories => {
  const newCategories = {};

  data.forEach(
    ({ post_category_id }) =>
      !newCategories[post_category_id] &&
      (newCategories[post_category_id] = { name: categoriesConfig[post_category_id], show: true }),
  );

  return newCategories;
};

export const NewsContext = createContext<{
  news: News[];
  categories: Categories;
  removedItems: News[];
  remove: (slug: string) => void;
  refetch: () => void;
  toggleCategory: (categoryId: string, show: boolean) => void;
  search: (query: string) => void;
}>(null);

const NewsProvider = ({ news, children }: NewsProviderProps) => {
  const [data, setData] = useState(news);
  const [categories, setCategories] = useState<Categories>(getCategories(news));
  const [removedItems, setRemovedItems] = useState<News[]>([]);
  const [query, setQuery] = useState('');

  useEffect(() => {
    setData(
      news.filter((item) => {
        return (
          categories[item.post_category_id]?.show &&
          !removedItems.find((removedItem) => removedItem.slug === item.slug) &&
          (query ? item.title.toLowerCase().indexOf(query.toLowerCase()) > -1 : true)
        );
      }),
    );
  }, [categories, removedItems, query]);

  const remove = useCallback(
    (slug: string) => {
      const newRemovedItems = [...removedItems, news.find((item) => item.slug === slug)];

      setRemovedItems(newRemovedItems);
      setCategories(
        getCategories(news.filter((item) => !newRemovedItems.find((removedItem) => removedItem.slug === item.slug))),
      );
    },
    [news, removedItems],
  );

  const refetch = useCallback(() => {
    setData(news);
    setRemovedItems([]);
    setCategories(getCategories(news));
  }, []);

  const toggleCategory = useCallback(
    (categoryId: string, show: boolean) =>
      setCategories((categories) => ({ ...categories, [categoryId]: { ...categories[categoryId], show } })),
    [],
  );

  const search = useCallback((query: string) => setQuery(query), []);

  return (
    <NewsContext.Provider value={{ news: data, categories, removedItems, remove, refetch, toggleCategory, search }}>
      {children}
    </NewsContext.Provider>
  );
};

export default NewsProvider;
